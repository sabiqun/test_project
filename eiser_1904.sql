-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 14, 2020 at 11:41 AM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `eiser_1904`
--

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

CREATE TABLE `banners` (
  `ban_id` bigint(20) UNSIGNED NOT NULL,
  `ban_title` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ban_subtitle` text COLLATE utf8mb4_unicode_ci,
  `ban_button` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ban_url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ban_photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ban_creator` int(11) NOT NULL,
  `ban_slug` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ban_status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`ban_id`, `ban_title`, `ban_subtitle`, `ban_button`, `ban_url`, `ban_photo`, `ban_creator`, `ban_slug`, `ban_status`, `created_at`, `updated_at`) VALUES
(1, 'Creative Shaper', 'Institute', 'add', 'www.it.com', NULL, 0, 'Ban5dba6f247a036', 0, '2019-10-31 18:20:36', '2019-11-01 19:15:07'),
(2, 'Advisor world', 'Institute', 'click', 'www.banner.com', NULL, 1, 'Ban5dba6f4a39390', 1, '2019-10-31 18:21:14', NULL),
(3, 'web tutorial', 'Institute', 'add', 'www.it.com', NULL, 3, 'title', 1, '2019-10-31 18:37:34', NULL),
(4, 'Creative Shaper', 'This is a IT <br> institute', 'add', 'www.banner.com', NULL, 2, 'creative-shaper', 1, '2019-10-31 18:38:44', NULL),
(5, 'Science project', 'All scholarship are included this feature.', 'add', '#bb', NULL, 4, 'science-project', 1, '2019-11-01 00:33:53', '2019-11-01 19:15:48'),
(6, 'Digital BD', 'Institute', 'add more', '#66', NULL, 4, 'digital-bd', 0, '2019-11-01 10:57:30', '2019-11-01 19:17:44'),
(7, 'Education', 'Institute', '44', 'add url', NULL, 4, 'education', 1, '2019-11-01 11:12:12', NULL),
(8, 'Creative world', 'Biggest Institute of BD', 'click', 'add url', '', 4, 'creative-world', 1, '2019-11-01 11:31:54', NULL),
(9, 'Creative world', 'Biggest Institute of BD', 'click', 'add url', 'banner_9_1572561459.png', 4, 'creative-world', 1, '2019-11-01 11:37:39', '2019-11-01 11:37:40');

-- --------------------------------------------------------

--
-- Table structure for table `basics`
--

CREATE TABLE `basics` (
  `basic_id` bigint(20) UNSIGNED NOT NULL,
  `basic_title` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `basic_subtitle` text COLLATE utf8mb4_unicode_ci,
  `basic_details` text COLLATE utf8mb4_unicode_ci,
  `basic_logo` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `basic_favicon` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `basic_flogo` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `basic_status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `basics`
--

INSERT INTO `basics` (`basic_id`, `basic_title`, `basic_subtitle`, `basic_details`, `basic_logo`, `basic_favicon`, `basic_flogo`, `basic_status`, `created_at`, `updated_at`) VALUES
(1, 'Eiser Ecommerce Bangladesh', NULL, 'fvnvbmv,', 'logo_1586811559.png', 'favicon_1573671456.png', 'flogo_1572781516.jpg', 1, '2019-11-03 17:00:00', '2020-04-13 20:59:20');

-- --------------------------------------------------------

--
-- Table structure for table `contact_information`
--

CREATE TABLE `contact_information` (
  `ci_id` bigint(20) UNSIGNED NOT NULL,
  `ci_phone1` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ci_phone2` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ci_phone3` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ci_phone4` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ci_email1` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ci_email2` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ci_email3` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ci_email4` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ci_add1` text COLLATE utf8mb4_unicode_ci,
  `ci_add2` text COLLATE utf8mb4_unicode_ci,
  `ci_add3` text COLLATE utf8mb4_unicode_ci,
  `ci_add4` text COLLATE utf8mb4_unicode_ci,
  `ci_status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contact_information`
--

INSERT INTO `contact_information` (`ci_id`, `ci_phone1`, `ci_phone2`, `ci_phone3`, `ci_phone4`, `ci_email1`, `ci_email2`, `ci_email3`, `ci_email4`, `ci_add1`, `ci_add2`, `ci_add3`, `ci_add4`, `ci_status`, `created_at`, `updated_at`) VALUES
(1, '01684567200', '99988540997', '004478654980', '9999888888888', 'infro@gmail.com', 'creative_56@yahoo.com', NULL, NULL, 'Dhanmondi,32/2,Dhaka,BD', NULL, NULL, NULL, 1, '2019-11-03 19:00:00', '2020-04-13 17:44:03');

-- --------------------------------------------------------

--
-- Table structure for table `contact_us`
--

CREATE TABLE `contact_us` (
  `conus_id` bigint(20) UNSIGNED NOT NULL,
  `conus_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `conus_phone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `conus_email` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `conus_sub` text COLLATE utf8mb4_unicode_ci,
  `conus_mess` text COLLATE utf8mb4_unicode_ci,
  `conus_slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `conus_status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contact_us`
--

INSERT INTO `contact_us` (`conus_id`, `conus_name`, `conus_phone`, `conus_email`, `conus_sub`, `conus_mess`, `conus_slug`, `conus_status`, `created_at`, `updated_at`) VALUES
(1, 'Mimi', '789065432', NULL, '1914 translation by H. Rackham', '\"On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, ', 'conus56789', 1, '2019-11-08 03:00:00', NULL),
(2, 'Titi', '56789432667', 'titi@gmil.com', 'The standard Lorem Ipsum passage, used since the 1500s', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum ', 'c678fr', 1, '2019-11-08 12:00:00', NULL),
(3, 'gopal', '0897346787', 'gopAL@GMAIL.COM', 'Cyclone', 'There are a number of structural characteristics common to all cyclones. A cyclone is a low-pressure area.[17] A cyclone\'s center (often known in a mature tropical cyclone as the eye),', 'con6745us', 1, '2019-11-10 05:00:00', NULL),
(4, 'Tuktuki', '0986543167', 'tuktuki@gmail.com', 'Formation', 'Extratropical cyclones begin as waves along weather fronts before occluding later in their life cycle as cold-core systems. However, some intense extratropical cyclones can become warm-core systems when a warm seclusion occurs.', 'hy76549', 1, '2019-11-10 14:00:00', NULL),
(5, 'Halum', '096543134', 'halu$@gmail.com', 'Surface-based types', 'A polar low is a small-scale, short-lived atmospheric low-pressure system (depression) that is found over the ocean areas poleward of the main polar front in both the Northern and Southern Hemispheres', 'hy76543ys', 1, '2019-11-10 11:00:00', NULL),
(6, 'srabony', NULL, 'onlysrabony.143@gmail.com', 'Assignment', 'hello', 'c5dcd028f60be1', 1, '2019-11-14 07:30:23', NULL),
(7, 'srabony', NULL, 'onlysrabony.143@gmail.com', 'food', 'hello', 'c5dcd02a58db11', 1, '2019-11-14 07:30:45', NULL),
(8, 'ndajknfkdmkvnj', NULL, 'dhdhj@gmail.com', 'fnbjdsnfj', 'bhbjsnjknsj', 'c5dcd0c513889f', 1, '2019-11-14 08:12:01', NULL),
(9, 'tuktuk', NULL, 'tuk@yahoo.com', 'project', 'hello jini', 'c5dcd0cac53a83', 1, '2019-11-14 08:13:32', NULL),
(10, 'riu roy', NULL, 'riu@hotmail.com', 'shaper', 'hello riu', 'c5dcd0d088668d', 1, '2019-11-14 08:15:04', NULL),
(11, 'srabony', NULL, 'morsalin@gmail.com', 'food', 'abcdef', 'c5e94d2f4208cf', 1, '2020-04-13 21:00:36', NULL),
(12, 'gronthy', NULL, 'gronthy@gmail.com', 'ART', 'hello', 'c5e94d44d6493c', 1, '2020-04-13 21:06:21', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_10_27_020703_create_banners_table', 2),
(5, '2019_11_02_012400_create_user_roles_table', 3),
(6, '2019_11_03_141005_create_social_media_table', 4),
(7, '2019_11_03_165631_create_basics_table', 5),
(8, '2019_11_03_174923_create_contact_information_table', 6),
(9, '2019_11_03_175649_create_contact_information_table', 7),
(10, '2019_11_07_202526_create_contact_us_table', 8),
(11, '2019_11_08_011400_create_contact_us_table', 9);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `social_media`
--

CREATE TABLE `social_media` (
  `sm_id` bigint(20) UNSIGNED NOT NULL,
  `sm_facebook` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sm_twitter` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sm_linkedin` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sm_youtube` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sm_google` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sm_pinterest` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sm_status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `social_media`
--

INSERT INTO `social_media` (`sm_id`, `sm_facebook`, `sm_twitter`, `sm_linkedin`, `sm_youtube`, `sm_google`, `sm_pinterest`, `sm_status`, `created_at`, `updated_at`) VALUES
(1, '#jkbkgnj khm lbm', '#', '#', '#', NULL, NULL, 1, NULL, '2020-04-13 17:43:41');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_id` int(11) NOT NULL DEFAULT '5',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `phone`, `email`, `email_verified_at`, `password`, `role_id`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Morsalin Khan', '', 'morsalin@gmail.com', NULL, '$2y$10$101LctD80uxwaiDhbd.G3uJXpWzPO/ofwegVcu/HTSkxLfxRKzlBq', 3, NULL, '2019-10-07 05:07:33', '2019-10-07 05:07:33'),
(2, 'Shahi Ahamed', '', 'shahi@gmail.com', NULL, '$2y$10$WaGwrnCptyZZCrlT8vAlkudZP2WrGkZoJZFnJWyUlDvZRAr.coyrS', 2, NULL, '2019-10-09 05:50:08', '2019-10-09 05:50:08'),
(3, 'Srabony', '', 'srabony@gmail.com', NULL, '$2y$10$lGoGRSsynDHUyX3u16R3IeWA/tDPdEVxzvOiEVznIFXhvDncUzhEW', 5, NULL, '2019-10-09 05:51:03', '2019-10-09 05:51:03'),
(4, 'srabony', '', 'sabiqunnahar1234@gmail.com', NULL, '$2y$10$z38gf95xGtL6FpipsOGHtOSCa6e.k7U0WChwXIBy3gLlBu.8A3cvm', 5, NULL, '2019-10-27 15:23:26', '2019-10-27 15:23:26'),
(5, 'Mita roy', '', 'mita@gmail.com', NULL, '$2y$10$GJOTGNY/F8KENgUeU0Nv1.O7rjucS/oQUeFbl5/9lHFS1916SXRtG', 1, NULL, '2019-11-02 09:45:53', '2019-11-02 09:45:53'),
(6, 'Rita', NULL, 'rita@gmail.com', NULL, '$2y$10$QMkYwMNummfIhrHgP9o43udAG1edjLzOF8bRk43tLBRxcX/4aI.oO', 5, NULL, '2019-11-02 19:58:01', '2019-11-02 19:58:01'),
(7, 'Sifat', '012345678707', 'sifat@gmail.com', NULL, '$2y$10$XRZV9i7quwHHbWQuHDP2AuWIV7QHC/tZ5xCMJ7bcBM7XfpMvdWBUK', 3, NULL, '2019-11-02 20:14:05', NULL),
(8, 'Nipa', '12345678444', 'nipa$@gamil.com', NULL, '$2y$10$F2Mf58qWPGsFEGKzxvi9y.d2dYaMPztaU4ZfKRGqfHdPyNrJyjAOS', 3, NULL, '2019-11-02 20:15:27', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `role_name` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_roles`
--

INSERT INTO `user_roles` (`role_id`, `role_name`, `role_status`, `created_at`, `updated_at`) VALUES
(1, 'Super Admin', 1, '2019-11-01 14:00:00', NULL),
(2, 'Admin', 1, '2019-11-01 16:00:00', NULL),
(3, 'Author', 1, '2019-11-01 12:00:00', NULL),
(4, 'Editor', 1, '2019-11-01 17:00:00', NULL),
(5, 'Subscriber', 1, '2019-11-01 10:00:00', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`ban_id`);

--
-- Indexes for table `basics`
--
ALTER TABLE `basics`
  ADD PRIMARY KEY (`basic_id`);

--
-- Indexes for table `contact_information`
--
ALTER TABLE `contact_information`
  ADD PRIMARY KEY (`ci_id`);

--
-- Indexes for table `contact_us`
--
ALTER TABLE `contact_us`
  ADD PRIMARY KEY (`conus_id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `social_media`
--
ALTER TABLE `social_media`
  ADD PRIMARY KEY (`sm_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`role_id`),
  ADD UNIQUE KEY `user_roles_role_name_unique` (`role_name`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `ban_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `basics`
--
ALTER TABLE `basics`
  MODIFY `basic_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `contact_information`
--
ALTER TABLE `contact_information`
  MODIFY `ci_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `contact_us`
--
ALTER TABLE `contact_us`
  MODIFY `conus_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `social_media`
--
ALTER TABLE `social_media`
  MODIFY `sm_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `user_roles`
--
ALTER TABLE `user_roles`
  MODIFY `role_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
